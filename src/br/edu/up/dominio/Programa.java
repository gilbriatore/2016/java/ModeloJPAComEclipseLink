package br.edu.up.dominio;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class Programa {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("ExemploJPA");
		EntityManager em = emf.createEntityManager();
		TypedQuery<Pessoa> query = em.createQuery("select p from Pessoa p", Pessoa.class);
		List<Pessoa> lista = query.getResultList();
		for (Pessoa pessoa : lista) {
			System.out.println("Nome: " + pessoa.getNome()); 
		}
		
	}
}